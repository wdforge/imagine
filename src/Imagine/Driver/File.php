<?php

/**
 * Microservice
 *
 * @package    Imagine\Driver
 * @version    1.0
 */
class Imagine_Driver_File extends Imagine_Driver_Base
{
    public function save($data, $filePath)
    {
        $result = false;
        $filePath = $this->_config['directory'] . DS . $filePath;

        if ($this->is_connect()) {
            if (file_exists($filePath) && !is_dir($filePath)) {

                $result = file_put_contents($filePath, $data);
            } else {

                $fileData = pathinfo($filePath);

                if (file_exists($fileData['dirname']) && !is_dir($fileData['dirname'])) {
                    $result = file_put_contents($filePath, $data);
                } else {
                    if (!file_exists($fileData['dirname'])) {
                        $old_mask = umask(0);
                        if ( !mkdir($fileData['dirname'], 0777, true) ) {
                            throw new Exception(sprintf('Не удалось создать директорию: %s', $fileData['dirname']));
                        }
                        chmod($fileData['dirname'], 0777);
                        umask($old_mask);
                    }

                    $result = file_put_contents($filePath, $data);
                }

                chmod($filePath, 0777);
            }
        } elseif (static::$retry < static::max_retry) {

            if ($this->connect()) {
                $result = $this->save($data, $filePath);
                static::$retry++;
            }
        }

        $emptystr = rvscdn($filePath);

        if (!empty($emptystr)) {
            throw new Exception(sprintf("File is not synchronized: %s", $filePath));
        }

        return $result;
    }

    public function load($filePath)
    {
        $filePath = $this->_config['directory'] . DS . $filePath;

        if (!empty($filePath)) {
            if ($filePath[0] == DS) {
                $filePath = substr($filePath, 1);
            }
            if ($filePath[strlen($filePath) - 1] == DS) {
                $filePath = substr($filePath, 0, -1);
            }
        }

        if (file_exists($filePath)) {
            return file_get_contents($filePath);
        }

        return false;
    }

    public function delete($filePath) {
        if(file_exists($filePath)) {
            delete_cdn_file($filePath);
        }
    }
}