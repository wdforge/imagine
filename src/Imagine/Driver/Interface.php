<?php

/**
 * Microservice
 *
 * @package    Imagine\Driver
 * @version    1.0
 */
interface Imagine_Driver_Interface
{
    public function connect();

    public function is_connect();

    public function disconnect();

    public function load($filePath);

    public function save($data, $filePath);

}