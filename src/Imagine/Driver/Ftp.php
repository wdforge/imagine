<?php

/**
 * Microservice
 *
 * @package    Imagine\Driver
 * @version    1.0
 */
class Imagine_Driver_Ftp extends Imagine_Driver_Base
{

    /**
     * @var \Ftp_Client\ftp
     */
    protected $_ftpClient = null;


    public function __construct($config = [], $autoconnect = true)
    {
        $this->_ftpClient = new \Ftp_Client\ftp($config);
        parent::__construct($config, $autoconnect);
    }

    public function connect()
    {

        if (!$this->is_connect()) {

            $this->_ftpClient->conn(
                $this->_config['hostname'],
                $this->_config['username'],
                $this->_config['password']
            );

            parent::connect();
        }

        return $this;
    }

    public function save($data, $filePath)
    {

        $tempFile = tempnam(sys_get_temp_dir(), 'ftp_');
        file_put_contents($tempFile, $data);

        $fileData = pathinfo($filePath);
        $result = $this->exists2($fileData['dirname']);


        if ($this->_ftpClient->put($filePath, $tempFile)) {
            unlink($tempFile);
            return true;
        }

        return false;
    }

    public function exists2($path)
    {
        return $this->_ftpClient->isDir($path, false);
    }

    public function load($filePath)
    {

        $fileData = pathinfo($filePath);
        $tempFile = tempnam(sys_get_temp_dir(), 'ftp_');

        if ($this->_ftpClient->get($tempFile, $filePath, 0)) {
            $content = file_get_contents($tempFile);
            return $content;
        } else {
            trigger_error(sprintf("FTP-Client: File: %s  not read", $filePath));
        }

        return false;
    }

    public function disconnect()
    {
        $this->_ftpClient->closeConn();
        parent::disconnect();
    }

    public function exists($path)
    {

        if (empty($path)) {
            return false;
        }

        $fileData = pathinfo($path);

        $path_dir = isset($fileData['dirname']) ? $fileData['dirname'] : $path;
        $path_file = isset($fileData['basename']) ? $fileData['basename'] : $path;

        $path = $path_dir;

        if ($path[0] == '/') {
            $path = substr($path, 1);
        }

        if ($path[strlen($path) - 1] == '/') {
            $path = substr($path, 0, -1);
        }

        $paths = explode('/', $path);

        $paths_result = [];

        $status = true;
        $path_complete = "";

        foreach ($paths as $path_num => $path_one) {

            if (empty($path_one)) {
                continue;
            }

            if (!$status) {
                break;
            }

            $path_complete = $path_complete . $path_one . ($path_num ? '/' : '');

            $fileList = $this->_ftpClient->listFiles($path_one, false);

            // проверка пути директорий
            if (($path_num + 2) < count($paths)) {

                $next_path = $paths[($path_num + 1)];

                if (!isset($paths[($path_num + 1)])) {
                    continue;
                }

                $cur_path = $paths[($path_num)];

                if (!empty($cur_path) && isset($fileList)) {

                    if (is_array($fileList)) {

                        $r1 = in_array($cur_path . '/' . $next_path, $fileList);
                        $r2 = in_array($cur_path, $fileList);

                        if (!$r1 && !$r2) {
                            $status = false;
                        }
                    }
                } else {
                    $status = false;
                }
            } else {

                //проверка файла
                if (is_array($fileList) && isset($path_file)) {
                    if (!in_array($path_file, $fileList)) {
                        $status = false;
                    }
                } else {
                    $status = false;
                }
            }

            $paths_result[] = $path_one;
        }

        return $status;
    }


}