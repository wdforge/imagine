<?php

/**
 * Microservice
 *
 * @package    Imagine\Driver
 * @version    1.0
 */
abstract class Imagine_Driver_Base implements Imagine_Driver_Interface
{

    const max_retry = 3;
    protected static $retry = 0;
    protected $_config = [];
    protected $_connected;
    protected $_hash;

    public function __construct($config = [], $autoconnect = true)
    {

        $this->_config = $config;
        $this->_connected = false;
        $this->_hash = static::getHashFromArray($this->_config);

        if ($autoconnect) {
            $this->connect();
        }
    }

    public static function getHashFromArray(array $settings = [])
    {
        return md5(implode(';', $settings));
    }

    public function connect()
    {

        if (!$this->is_connect()) {
            $this->_connected = true;
        }

        return $this;
    }

    public function is_connect()
    {
        return $this->_connected;
    }

    abstract public function load($filePath);

    abstract public function save($data, $filePath);
    abstract public function delete($filePath);
    public function disconnect()
    {
        $this->_connected = false;
    }

    public function checkHash($hash)
    {

        if ($this->_hash == $hash) {
            return true;
        }

        return false;
    }

    public function getHash()
    {
        return $this->_hash;
    }

}