<?php

/**
 * Microservice
 * 
 * @package    Imagine/Service
 * @version    1.0
 */


class Imagine_Service extends Abstract_Application {

	public function OnUseImagineService(&$event) {
        $event->result = true;
	}

}
