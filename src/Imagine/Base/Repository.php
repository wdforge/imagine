<?php

/**
 * Microservice
 *
 * @package    Imagine/Base/Repository
 * @version    1.0
 */
class Imagine_Base_Repository extends Db_Zend_Repository
{
    /**
     * @var \Intervention\Image\ImageManager
     */
    protected $__imageManagerSrc;
    protected $__imageDrivers;

    public function __construct($params)
    {
        $cfg = self::getConfig();

        $this->__imageManagerSrc = new \Intervention\Image\ImageManager(
            is_array($cfg['imagine']['image-manager']) ?
                $cfg['imagine']['image-manager'] : []
        );

        $this->__imageDrivers = null;
        parent::__construct($params);
    }

    /**
     * @return Intervention\Image\ImageManager
     */
    public function getImageManager()
    {
        return $this->__imageManagerSrc;
    }

    /**
     * LazyLoad:
     * Получает "драйвер" подключения по секции
     * если тоже самое подключение, уже было к другим секциям,
     * использует существующе.
     * Если подключение к указаной секции уже было, просто возвращает соединение(драйвер).
     *
     * @return Imagine_Driver_Interface
     */
    public function getDriver($section = "original")
    {
        if (!empty($this->__imageDrivers[$section])) {
            return $this->__imageDrivers[$section];
        }

        try {

            if ($settings = $this->getItemSettings()) {

                if (!empty($settings['file-storage'][$section]['driver']['class'])) {

                    if (!isset($settings['file-storage'][$section]['driver']['connect'])) {
                        throw new Exception("Please add to config driver:\"connect\" section");
                    }

                    $classDriver = $settings['file-storage'][$section]['driver']['class'];
                    $classConnect = $settings['file-storage'][$section]['driver']['connect'];

                    if ($driver = $this->getDriverByHash(
                        Imagine_Driver_Base::getHashFromArray($classConnect))

                    ) {
                        $this->__imageDrivers[$section] = $driver;
                    } else {
                        $this->__imageDrivers[$section] = new $classDriver($classConnect);
                    }

                    return $this->__imageDrivers[$section];
                } else {
                    throw new \Exception(sprintf("Imagine repository \"%s\": driver for storage: class is not set", get_class($this)));
                }
            }
        } catch (\Exception $e) {
            throw $e;
        }

        return false;
    }

    /**
     * Проверка через hash использования соединения ранее
     *
     * @return Imagine_Driver_Base | bool (false)
     */
    public function getDriverByHash($hash)
    {
        if (!empty($this->__imageDrivers)) {
            foreach ($this->__imageDrivers as $section => $driver) {
                if ($driver->checkHash($hash)) {
                    return $driver;
                }
            }
        }
        return false;
    }

    /**
     * Возвращает прописанную в настройках директорию, проверяет её существование,
     * по возможности, пытается создать.
     *
     * @param $section имя профиля настроек типа картинок
     *
     * @return string | bool (false)
     */
    public function getDirectory($section = "original")
    {
        $settings = $this->getItemSettings();

        if (!empty($settings['file-storage'][$section]['directory'])) {
            return $settings['file-storage'][$section]['directory'];
        }
    }

    /**
     * Возвращает картинки со временным статусом.
     *
     * @param $company_id идентификатор компании
     * @return string | bool (false)
     */
    public function getTempraryItems($company_id = 0) {
        return $this->findAll([
            'where' => [
                'is_temprary' => 1,
                'company_id' => $company_id,
            ],
        ]);
    }

    /**
     * Удаляет картинки со временным статусом.
     *
     * @param $company_id идентификатор компании
     * @return string | bool (false)
     */
    public function cleanTemprary($company_id) {

        $items = $this->getTempraryItems($company_id);
        $del_count = 0;

        foreach($items as $item) {
            if($item->remove()) {
                $del_count++;
            }
        }

        return $del_count;
    }

    public function loadUrl($url)
    {
        if ( ! isset($url)) {
            return false;
        }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_TIMEOUT, 5);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_URL, $url);

        $response = curl_exec($ch);

        switch ($http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE)) {
            case 200:
                break;
            default:
                error_log(sprintf("Url: %s\n".'Неожиданный код HTTP: %s, %s', $url, $http_code, __LINE__));
                break;
        }

        curl_close($ch);
        return $response;
    }

}

