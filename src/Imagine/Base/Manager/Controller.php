<?php

/**
 * Microservice
 *
 * @package    Imagine/Base/Controller
 * @version    1.0
 */
class Imagine_Base_Manager_Controller extends Action_Controller
{
    /**
     * @var Imagine_Base_EntityRepository
     */
    protected $repository;
    protected $classEntity = "Item_Imagine_Base_Item";

    public function initRepository($classEntity)
    {
        $this->classEntity = $classEntity;
        $this->repository = $this
            ->getServiceManager()
            ->getRepository($this->classEntity);
    }

    /**
     * Получение одного объекта
     *
     * @param integer
     * @return Item_Imagine_Company_Logo
     */
    public function getByIDAction($id)
    {
        return $this->getRepository()->getByID($id);
    }

    /**
     *
     * @param none
     *
     * @return  Imagine_Base_Repository
     */
    public function getRepository()
    {
        return $this->repository;
    }

    /**
     * Запись одного объекта
     *
     * @param integer
     *
     * @param Item_Imagine_Base_Item $image
     * @return bool
     */
    public function updateAction($id, $image)
    {
        if ($image instanceof Item_Imagine_Base_Item) {

            return $this->getRepository()->updateRow(
                $id, [
                    'originalFile' => $image->getOriginalInstance()->basename,
                    'largeFile' => $image->getLargeInstance()->basename,
                    'smallFile' => $image->getSmallInstance()->basename,
                    'middleFile' => $image->getMiddleInstance()->basename,
                    'settings' => json_encode($image->getSettings(), true)
                ]
            );
        }

        return false;
    }

    /**
     * Вставка одного объекта
     *
     * @param integer
     * @param Item_Imagine_Base_Item $image
     * @return bool
     */
    public function insertAction($image)
    {
        if ($image instanceof Item_Imagine_Base_Item) {

            $image
                ->getOriginalInstance()
                ->save();

            return $this->getRepository()->insertRow(
                [
                    'originalFile' => $image->getOriginalInstance()->basename,
                    'largeFile' => $image->getLargeInstance()->basename,
                    'smallFile' => $image->getSmallInstance()->basename,
                    'middleFile' => $image->getMiddleInstance()->basename,
                    'settings' => json_encode($image->getSettings(), true)
                ]
            );
        }

        return false;
    }
}