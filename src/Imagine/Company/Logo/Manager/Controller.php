<?php

/**
 * Microservice
 *
 * @package    Imagine/Company/Logo/Controller
 * @version    1.0
 */
class Imagine_Company_Logo_Manager_Controller extends Imagine_Base_Manager_Controller
{
    /**
     * @var Imagine_Company_Fon_Repository
     */
    protected $repository;

    protected $_requestFilter = [

        'getByIDAction' => [
            '__isRequest' => false,
            'id' => [
                'from' => 'POST',
                'type' => 'integer',
                'regexp' => '/^\d+$/',
                'default' => 0,
                'error' => 'Request param "id" is not valid set.',
            ],
        ],

        'uploadAction' => [
            '__isRequest' => false,

            'id' => [
                'from' => 'POST',
                'type' => 'integer',
                'regexp' => '/^\d+$/',
                'default' => 0,
                'error' => 'Request param "id" is not valid set.',
            ],

            'filename' => [
                'from' => 'POST',
                'type' => 'string',
                'default' => 'file111.jpg',
                'error' => 'Request param "filename" is not valid set.',
            ],

            'content' => [
                'from' => 'POST',
                'error' => 'Request param "content" is not valid set.',
            ],

            'company_id' => [
                'from' => 'POST',
                'default' => '1234',
                'error' => 'Request param "company_id" is not valid set.',
            ],

            'filter_settings' => [
                'from' => 'POST',
                'default' => 0,
                'error' => 'Request param "filter_settings" is not valid set.',
            ],

        ],
    ];

    public function __construct($params = [])
    {
        $this->initRepository('Item_Imagine_Company_Logo');
        parent::__construct($params);
    }

    public function uploadAction($id = null, $filename, $content, $company_id, $filter_settings)
    {
        $classItem = $this->getRepository()->getItemClass();
        $itemSettings = $this->getRepository()->getItemSettings();
        $itemSettings['company_id'] = $company_id;

        /**
         * var Item_Imagine_Base_Item $classItem
         */
        $item = new $classItem([
            'originalFile' => $filename,
            'smallFile' => $filename,
            'largeFile' => $filename,
            'middleFile' => $filename,
            'company_id' => $company_id,
            'settings' => json_encode($itemSettings)
        ],
            $this->getRepository(),
            $this->getRepository()->getItemSettings()
        );

        if ($id) {
            $item->imagine_id = $id;
        }

        if (!empty($filter_settings)) {
            foreach (['small', 'middle', 'large'] as $profile) {
                $item->setFilterProfileSettings($profile, $filter_settings);
            }
        }

        // переменные для пути либо чего то ещё
        $item->setVariable('company_id', $company_id);

        // заполнение указателя на профиль original
        $item->setProfileImageContent('original', $content);

        // масштабирование и заполнение указателей на профили картинок
        $item->setResizeSettingsImageProfiles();

        // применение настроенных фильтров
        $item->setFilterSettingsImageProfiles();
        $item->setTemprary(false);

        // сохранение записи и файлов
        return $item->save();
    }

    /**
     * Получение одного объекта
     *
     * @param integer
     * @return Item_Imagine_Company_Logo
     */
    public function getByIDAction($id)
    {
        return $this->getRepository()->getByID($id);
    }

}