<?php

/**
 * Microservice
 *
 * @package    Imagine
 * @version    1.0
 */

global $dbPdo, $dbYmarket, $solrYmarket, $solr, $ftpCdn, $cdnSettings;

return [

    // пути для автозагрузки классов
    'autoload_paths' => [
        // локальные пути сервиса
        realpath(dirname(__FILE__) . '/src/'),
    ],

    // настройка для инициализации репозитория и его получение через ServiceManager
    'repositories' => [

        'Item_Imagine_Company_Fon' => [
            'class' => 'Imagine_Company_Fon_Repository',
            'params' => [
                'connect' => $dbYmarket,
                'table' => 'imagine_fon',
                'field_id' => 'imagine_id',

                // TODO: секция дополнительных настроек объектов
                'element-settings' => [

                    // настройки хранения и пост-обработки файлов
                    'file-storage' => [

                        'original' => [

							'driver' => [
								'class'   =>'Imagine_Driver_File',
								'connect' => $cdnSettings,
							],

                            'title' => 'Оригинальные',

                            'directory' => 'company/|company_id|/fon/original/',
							'hosturl' =>  $cdnSettings['hostPathImages'],
                        ],

                        'small' => [

							'driver' => [
								'class' => 'Imagine_Driver_File',
								'connect' => $cdnSettings,
							],

                            'title' => 'Маленькие',

                            'directory' => 'company/|company_id|/fon/small/',
							'hosturl' =>  $cdnSettings['hostPathImages'],

                            'width' => 150,

                            'filter' => [
                            ],

                            'background' => '#fff'
                        ],

                        'middle' => [

							'driver' => [
								'class' => 'Imagine_Driver_File',
								'connect' => $cdnSettings,
							],

                            'title' => 'Средние',

                            'directory' => 'company/|company_id|/fon/middle/',
							'hosturl' =>  $cdnSettings['hostPathImages'],

                            'width' => 350,

                            'filter' => [
                            ],

                            'background' => '#ccc'
                        ],

                        'large' => [

							'driver' => [
								'class' => 'Imagine_Driver_File',
								'connect' => $cdnSettings,
							],

                            'title' => 'Большие',

                            'directory' => 'company/|company_id|/fon/large/',
							'hosturl' =>  $cdnSettings['hostPathImages'],

                            'width' => 700,

                            'filter' => [
                            ],

                            'background' => '#ddd'
                        ],
                    ],
                ],
            ],
        ],

        'Item_Imagine_Company_Logo' => [
            'class' => 'Imagine_Company_Logo_Repository',

            'params' => [

                'connect' => $dbYmarket,

                'table' => 'imagine_logo',
                'field_id' => 'imagine_id',

                // TODO: секция дополнительных настроек объектов
                'element-settings' => [

                    // настройки хранения и пост-обработки файлов
                    'file-storage' => [

                        'original' => [

							'driver' => [
								'class' => 'Imagine_Driver_File',
								'connect' => $cdnSettings,
							],

                            'title' => 'Оригинальные',

                            'directory' => '/company/|company_id|/logo/original/',
							'hosturl' =>  $cdnSettings['hostPathImages'],
                        ],

                        'small' => [

							'driver' => [
								'class' => 'Imagine_Driver_File',
								'connect' => $cdnSettings,
							],

                            'title' => 'Маленькие',

                            'directory' => '/company/|company_id|/logo/small/',
							'hosturl' =>  $cdnSettings['hostPathImages'],

                            'width' => 150,

                            'filter' => [
                            ],

                            'background' => '#fff'
                        ],

                        'middle' => [

							'driver' => [
								'class' => 'Imagine_Driver_File',
								'connect' => $cdnSettings,
							],

                            'title' => 'Средние',
                            'directory' => '/company/|company_id|/logo/middle/',
							'hosturl' =>  $cdnSettings['hostPathImages'],
                            'width' => 350,

                            'filter' => [
                            ],

                            'background' => '#ccc'
                        ],

                        'large' => [

							'driver' => [
								'class' => 'Imagine_Driver_File',
								'connect' => $cdnSettings,
							],

                            'title' => 'Большие',
                            'directory' => '/company/|company_id|/logo/large/',
							'hosturl' =>  $cdnSettings['hostPathImages'],

                            'width' => 700,

                            'filter' => [
                            ],

                            'background' => '#ddd'
                        ],
                    ],
                ],
            ],
        ],

        'Item_Imagine_Company_GoodImage' => [
            'class' => 'Imagine_Company_GoodImage_Repository',

            'params' => [

                'connect' => $dbYmarket,

                'table' => 'imagine_good_image',
                'field_id' => 'imagine_id',

                // TODO: секция дополнительных настроек объектов
                'element-settings' => [

                    // настройки хранения и пост-обработки файлов
                    'file-storage' => [

                        'original' => [

                            'driver' => [
                                'class' => 'Imagine_Driver_File',
                                'connect' => $cdnSettings,
                            ],

                            'title' => 'Оригинальные',

                            'directory' => '/company/|company_id|/good/original/',
                            'hosturl' =>  $cdnSettings['hostPathImages'],
                        ],

                        'small' => [

                            'driver' => [
                                'class' => 'Imagine_Driver_File',
                                'connect' => $cdnSettings,
                            ],

                            'title' => 'Маленькие',

                            'directory' => '/company/|company_id|/good/small/',
                            'hosturl' =>  $cdnSettings['hostPathImages'],

                            'width' => 150,

                            'filter' => [
                            ],

                            'background' => '#fff'
                        ],

                        'middle' => [

                            'driver' => [
                                'class' => 'Imagine_Driver_File',
                                'connect' => $cdnSettings,
                            ],

                            'title' => 'Средние',
                            'directory' => '/company/|company_id|/good/middle/',
                            'hosturl' =>  $cdnSettings['hostPathImages'],
                            'width' => 350,

                            'filter' => [
                            ],

                            'background' => '#ccc'
                        ],

                        'large' => [

                            'driver' => [
                                'class' => 'Imagine_Driver_File',
                                'connect' => $cdnSettings,
                            ],

                            'title' => 'Большие',
                            'directory' => '/company/|company_id|/good/large/',
                            'hosturl' =>  $cdnSettings['hostPathImages'],

                            'width' => 700,

                            'filter' => [
                            ],

                            'background' => '#ddd'
                        ],
                    ],
                ],
            ],
        ],

        'Item_Imagine_Company_SubGoodImage' => [
            'class' => 'Imagine_Company_SubGoodImage_Repository',
            'params' => [

                'connect' => $dbYmarket,

                'table' => 'imagine_sub_good_image',
                'field_id' => 'imagine_id',

                // TODO: секция дополнительных настроек объектов
                'element-settings' => [

                    // настройки хранения и пост-обработки файлов
                    'file-storage' => [

                        'original' => [

                            'driver' => [
                                'class' => 'Imagine_Driver_File',
                                'connect' => $cdnSettings,
                            ],

                            'title' => 'Оригинальные',

                            'directory' => '/company/|company_id|/sub-good/original/',
                            'hosturl' =>  $cdnSettings['hostPathImages'],
                        ],

                        'small' => [

                            'driver' => [
                                'class' => 'Imagine_Driver_File',
                                'connect' => $cdnSettings,
                            ],

                            'title' => 'Маленькие',

                            'directory' => '/company/|company_id|/sub-good/small/',
                            'hosturl' =>  $cdnSettings['hostPathImages'],

                            'width' => 150,

                            'filter' => [
                            ],

                            'background' => '#fff'
                        ],

                        'middle' => [

                            'driver' => [
                                'class' => 'Imagine_Driver_File',
                                'connect' => $cdnSettings,
                            ],

                            'title' => 'Средние',
                            'directory' => '/company/|company_id|/sub-good/middle/',
                            'hosturl' =>  $cdnSettings['hostPathImages'],
                            'width' => 350,

                            'filter' => [
                            ],

                            'background' => '#ccc'
                        ],

                        'large' => [

                            'driver' => [
                                'class' => 'Imagine_Driver_File',
                                'connect' => $cdnSettings,
                            ],

                            'title' => 'Большие',
                            'directory' => '/company/|company_id|/sub-good/large/',
                            'hosturl' =>  $cdnSettings['hostPathImages'],

                            'width' => 700,

                            'filter' => [
                            ],

                            'background' => '#ddd'
                        ],
                    ],
                ],
            ],
        ],
    ],

    'imagine' => [
        'image-manager' => [
            'driver' => 'gd' // or 'imagick'
        ]
    ],

];
